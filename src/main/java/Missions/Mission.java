package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission {
  protected Coordinates targetCoordinates;
  protected String pilotName;
  protected AerialVehicle aircraft;

  public Mission(Coordinates targetCoordinates, String pilotName, AerialVehicle aircraft)
      throws AerialVehicleNotCompatibleException {
    if (!isValidForMission(aircraft)) {
      throw new AerialVehicleNotCompatibleException(
          "Aircraft "
              + aircraft.getClass().getSimpleName()
              + " is not suitable for "
              + this.getClass().getSimpleName());
    }

    this.targetCoordinates = targetCoordinates;
    this.pilotName = pilotName;
    this.aircraft = aircraft;
  }

  public void begin() {
    aircraft.flyTo(targetCoordinates);
    System.out.println("Beginning mission!");
  }

  public void cancel() {
    aircraft.land();
    System.out.println("Aborting mission!");
  }

  public void finish() {
    System.out.println(execute());
    System.out.println("Mission Finished!");
    aircraft.land();
  }

  protected abstract String execute();

  protected abstract boolean isValidForMission(AerialVehicle aircraft);
}
