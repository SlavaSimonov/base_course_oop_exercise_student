package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.AttackAircraft;
import Entities.Coordinates;

public class AttackMission extends Mission {

  private final String targetName;

  public AttackMission(
      String targetName, Coordinates targetCoordinates, String pilotName, AerialVehicle aircraft)
      throws AerialVehicleNotCompatibleException {
    super(targetCoordinates, pilotName, aircraft);
    this.targetName = targetName;
  }

  @Override
  protected String execute() {
    AttackAircraft attacker = (AttackAircraft) super.aircraft;
    return super.pilotName
        + ": "
        + super.aircraft.getClass().getSimpleName()
        + " Attacking suspect "
        + targetName
        + " with "
        + attacker.getRocket()
        + "X"
        + attacker.getRocketCount();
  }

  @Override
  protected boolean isValidForMission(AerialVehicle aircraft) {
    return aircraft instanceof AttackAircraft;
  }
}
