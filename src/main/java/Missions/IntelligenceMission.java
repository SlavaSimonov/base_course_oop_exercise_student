package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.BDAAircraft;
import AerialVehicles.IntelligenceAircraft;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {

  private final String region;

  public IntelligenceMission(
      String region, Coordinates targetCoordinates, String pilotName, AerialVehicle aircraft)
      throws AerialVehicleNotCompatibleException {
    super(targetCoordinates, pilotName, aircraft);
    this.region = region;
  }

  @Override
  protected String execute() {
    IntelligenceAircraft intel = (IntelligenceAircraft) super.aircraft;
    return super.pilotName
        + ": "
        + super.aircraft.getClass().getSimpleName()
        + " Collecting data in "
        + region
        + " with "
        + intel.getSensor();
  }

  @Override
  protected boolean isValidForMission(AerialVehicle aircraft) {
    return aircraft instanceof BDAAircraft;
  }
}
