package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.BDAAircraft;
import Entities.Coordinates;

public class BDAMission extends Mission {

  private final String objective;

  public BDAMission(
      String objective, Coordinates targetCoordinates, String pilotName, AerialVehicle aircraft)
      throws AerialVehicleNotCompatibleException {
    super(targetCoordinates, pilotName, aircraft);
    this.objective = objective;
  }

  @Override
  protected String execute() {
    BDAAircraft bda = (BDAAircraft) super.aircraft;
    return super.pilotName
        + ": "
        + super.aircraft.getClass().getSimpleName()
        + " Taking pictures of "
        + objective
        + " with "
        + bda.getCamera();
  }

  @Override
  protected boolean isValidForMission(AerialVehicle aircraft) {
    return aircraft instanceof BDAAircraft;
  }
}
