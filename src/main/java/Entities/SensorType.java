package Entities;

public enum SensorType {
    INFRA_RED{
        @Override
        public String toString() {
            return "Infra Red";
        }
    },
    ELINT{
        @Override
        public String toString() {
            return "Elint";
        }
    },
}
