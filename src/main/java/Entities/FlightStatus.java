package Entities;

public enum FlightStatus {
    READY,
    NOT_READY,
    IN_AIR,
}
