package Entities;

public enum RocketType {
  PYTHON {
    @Override
    public String toString() {
      return "Python";
    }
  },
  AMRAM{
    @Override
    public String toString() {
      return "Amram";
    }
  },
  SPICE250{
    @Override
    public String toString() {
      return "Spice250";
    }
  }
}
