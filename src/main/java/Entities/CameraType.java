package Entities;

public enum CameraType {
  REGULAR {
    @Override
    public String toString() {
      return "Regular";
    }
  },
  THERMAL {
    @Override
    public String toString() {
      return "Thermal";
    }
  },
  NIGHT_VISION {
    @Override
    public String toString() {
      return "Night vision";
    }
  }
}
