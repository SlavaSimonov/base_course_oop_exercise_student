import AerialVehicles.*;
import Entities.CameraType;
import Entities.Coordinates;
import Entities.RocketType;
import Entities.SensorType;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BDAMission;
import Missions.IntelligenceMission;

public class Main {

  public static void main(String[] args) {
    try {
      scenario1();
      scenario2();
      scenario3();
    } catch (AerialVehicleNotCompatibleException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void scenario1() throws AerialVehicleNotCompatibleException {
    System.out.println("MISSION 1");

    Coordinates homeBase = new Coordinates(12.0, 15.2);
    F16 f16 = new F16(5, RocketType.PYTHON, CameraType.THERMAL, homeBase);
    f16.flyTo(new Coordinates(2.0, 3.0));

    BDAMission bdaMission = new BDAMission("house", new Coordinates(3.0, 5.0), "Dan", f16);
    bdaMission.begin();
    bdaMission.finish();
  }

  private static void scenario2() throws AerialVehicleNotCompatibleException {
    System.out.println("MISSION 2");

    Coordinates homeBase = new Coordinates(12.0, 15.2);
    Zik zik = new Zik(CameraType.REGULAR, SensorType.INFRA_RED, homeBase);
    Kochav kochav =
        new Kochav(5, RocketType.SPICE250, CameraType.THERMAL, SensorType.ELINT, homeBase);

    AttackMission attackMission;
    try {
      attackMission = new AttackMission("plant", new Coordinates(4.0, 3.0), "Noam", zik);
    } catch (Exception e) {
      System.out.println(e.getMessage());
      attackMission = new AttackMission("plant", new Coordinates(4.0, 3.0), "Shaked", kochav);
    }

    attackMission.begin();
    attackMission.finish();
  }

  private static void scenario3() throws AerialVehicleNotCompatibleException {
    System.out.println("MISSION 3");

    Coordinates homeBase = new Coordinates(12.0, 15.2);
    Shoval shoval =
        new Shoval(5, RocketType.PYTHON, CameraType.THERMAL, SensorType.INFRA_RED, homeBase);

    shoval.hoverOverLocation(new Coordinates(10.0, 10.0));

    IntelligenceMission intelligenceMission =
        new IntelligenceMission("gaza", new Coordinates(3.0, 5.0), "Moshe", shoval);
    intelligenceMission.begin();
    intelligenceMission.finish();
  }
}
