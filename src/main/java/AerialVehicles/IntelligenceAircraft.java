package AerialVehicles;

import Entities.SensorType;

public interface IntelligenceAircraft {
    SensorType getSensor();
}
