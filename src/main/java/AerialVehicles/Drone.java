package AerialVehicles;

import Entities.Coordinates;
import Entities.FlightStatus;

public abstract class Drone extends AerialVehicle {

  public Drone(Coordinates homeBase) {
    super(homeBase);
  }

  public String hoverOverLocation(Coordinates destination) {
    flightStatus = FlightStatus.IN_AIR;
    String output = "Hovering over: " + destination.toString();

    System.out.println(output);
    return output;
  }
}
