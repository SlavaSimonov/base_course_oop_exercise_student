package AerialVehicles;

import Entities.CameraType;
import Entities.Coordinates;
import Entities.SensorType;

public class Zik extends HermesDrone implements IntelligenceAircraft, BDAAircraft {

  private final CameraType cameraType;
  private final SensorType sensorType;

  public Zik(CameraType cameraType, SensorType sensorType, Coordinates homeBase) {
    super(homeBase);
    this.cameraType = cameraType;
    this.sensorType = sensorType;
  }

  @Override
  public CameraType getCamera() {
    return cameraType;
  }

  @Override
  public SensorType getSensor() {
    return sensorType;
  }
}
