package AerialVehicles;

import Entities.Coordinates;
import Entities.RocketType;
import Entities.SensorType;

public class F15 extends Airplane implements AttackAircraft, IntelligenceAircraft {

  private final int rocketCount;
  private final RocketType rocketType;
  private final SensorType sensorType;

  public F15(int rocketCount, RocketType rocketType, SensorType sensorType, Coordinates homeBase) {
    super(homeBase);
    this.rocketCount = rocketCount;
    this.rocketType = rocketType;
    this.sensorType = sensorType;
  }

  @Override
  public int getRocketCount() {
    return rocketCount;
  }

  @Override
  public RocketType getRocket() {
    return rocketType;
  }

  @Override
  public SensorType getSensor() {
    return sensorType;
  }
}
