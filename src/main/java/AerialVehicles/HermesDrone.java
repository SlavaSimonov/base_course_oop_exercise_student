package AerialVehicles;

import Entities.Coordinates;

public abstract class HermesDrone extends Drone{

  private static final int HERMES_MAX_HOURS_BETWEEN_REPAIRS = 100;

  public HermesDrone(Coordinates homeBase) {
    super(homeBase);
  }

  @Override
  protected int getMaxHoursBetweenRepairs() {
    return HERMES_MAX_HOURS_BETWEEN_REPAIRS;
  }
}
