package AerialVehicles;

import Entities.CameraType;

public interface BDAAircraft {
    CameraType getCamera();
}
