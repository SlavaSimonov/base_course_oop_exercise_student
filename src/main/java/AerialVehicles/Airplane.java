package AerialVehicles;

import Entities.Coordinates;

public abstract class Airplane extends AerialVehicle {

  private static final int MAX_HOURS_BETWEEN_REPAIRS = 250;

  public Airplane(Coordinates homeBase) {
    super(homeBase);
  }

  @Override
  protected int getMaxHoursBetweenRepairs() {
    return MAX_HOURS_BETWEEN_REPAIRS;
  }
}
