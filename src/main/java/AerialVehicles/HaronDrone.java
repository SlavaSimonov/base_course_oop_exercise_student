package AerialVehicles;

import Entities.Coordinates;

public abstract class HaronDrone extends Drone{

  private static final int HARON_MAX_HOURS_BETWEEN_REPAIRS = 150;

  public HaronDrone(Coordinates homeBase) {
    super(homeBase);
  }

  @Override
  protected int getMaxHoursBetweenRepairs() {
    return HARON_MAX_HOURS_BETWEEN_REPAIRS;
  }
}
