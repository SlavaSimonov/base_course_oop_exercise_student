package AerialVehicles;

import Entities.CameraType;
import Entities.Coordinates;
import Entities.RocketType;
import Entities.SensorType;


public class Shoval extends HaronDrone
    implements AttackAircraft, IntelligenceAircraft, BDAAircraft {

  private final int rocketCount;
  private final RocketType rocketType;
  private final CameraType cameraType;
  private final SensorType sensorType;

  public Shoval(int rocketCount, RocketType rocketType, CameraType cameraType, SensorType sensorType, Coordinates homeBase) {
    super(homeBase);
    this.rocketCount = rocketCount;
    this.rocketType = rocketType;
    this.cameraType = cameraType;
    this.sensorType = sensorType;
  }

  @Override
  public int getRocketCount() {
    return rocketCount;
  }

  @Override
  public RocketType getRocket() {
    return rocketType;
  }

  @Override
  public CameraType getCamera() {
    return cameraType;
  }

  @Override
  public SensorType getSensor() {
    return sensorType;
  }
}
