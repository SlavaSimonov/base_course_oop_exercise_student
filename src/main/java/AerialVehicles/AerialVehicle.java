package AerialVehicles;

import Entities.Coordinates;
import Entities.FlightStatus;

public abstract class AerialVehicle {
  protected int hoursFromRepair = 0;
  protected FlightStatus flightStatus = FlightStatus.READY;
  protected Coordinates homeBase;

  public AerialVehicle(Coordinates homeBase) {
    this.homeBase = homeBase;
  }

  public void flyTo(Coordinates destination) {
    if (flightStatus.equals(FlightStatus.NOT_READY)) {
      System.out.println("Aerial vehicle isn't ready to fly ");
    } else {
      System.out.println(
          "Flying to: " + destination.toString());
      flightStatus = FlightStatus.IN_AIR;
    }
  }

  public void land(Coordinates destination) {
    System.out.println(
        "Landing on: " + destination.toString());
    check();
  }

  public void land() {
    land(homeBase);
  }

  protected void check() {
    if (hoursFromRepair >= getMaxHoursBetweenRepairs()) {
      flightStatus = FlightStatus.NOT_READY;
      repair();
    }else {
      flightStatus = FlightStatus.READY;
    }
  }

  protected final void repair() {
    hoursFromRepair = 0;
    flightStatus = FlightStatus.READY;
  }

  protected abstract int getMaxHoursBetweenRepairs();
}
