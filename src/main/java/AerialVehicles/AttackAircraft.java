package AerialVehicles;

import Entities.RocketType;

public interface AttackAircraft {
    int getRocketCount();
    RocketType getRocket();
}
