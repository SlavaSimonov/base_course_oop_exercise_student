package AerialVehicles;


import Entities.CameraType;
import Entities.Coordinates;
import Entities.RocketType;

public class F16 extends Airplane implements AttackAircraft, BDAAircraft{

  private final int rocketCount;
  private final RocketType rocketType;
  private final CameraType cameraType;

  public F16(int rocketCount, RocketType rocketType, CameraType cameraType, Coordinates homeBase) {
    super(homeBase);
    this.rocketCount = rocketCount;
    this.rocketType = rocketType;
    this.cameraType = cameraType;
  }

  @Override
  public int getRocketCount() {
    return rocketCount;
  }

  @Override
  public RocketType getRocket() {
    return rocketType;
  }

  @Override
  public CameraType getCamera() {
    return cameraType;
  }
}
